import org.hibernate.Session;

import java.util.Optional;

public class ClientDAO {

    private final Session session;

    public ClientDAO(Session session) {
        this.session = session;
    }

    public Optional<Client> getById(Long id) {
       Client result = session.load(Client.class, id);
       return Optional.of(result);
    }
}
